pieces = [1,2,5,10,20,50,100,200]

def renduMonnaie(montant):
    return renduMonnaieRecur(montant,[], len(pieces)-1)

def renduMonnaieRecur(montant,rendu,indPiece):
    if montant == 0 :
        return rendu
    while montant >= pieces[indPiece]:
        montant -= pieces[indPiece]
        rendu.append(pieces[indPiece])

    return renduMonnaieRecur(montant,rendu,indPiece-1)



print (renduMonnaie(500))

